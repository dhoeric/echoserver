# echoserver

For echo request info and environment inside the container, suitable for testing on docker orchestration platforms, e.g. ECS / Kubernetes / docker swarm

## Usage
The echoserver will expose port 8080 for listening HTTP request.
```
docker run --rm -it -p 8080:8080 dhoeric/echoserver 
```
Then open http://localhost:8080

