package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"sort"
	"time"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func main() {
	http.HandleFunc("/", defaultHandler)
	http.ListenAndServe(fmt.Sprintf(":%s", getEnv("PORT", "8080")), nil)
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	// For stdout
	curTime := time.Now().Format("2006-01-02T15:04:05.000000000Z07:00")
	fmt.Println(fmt.Sprintf("%s %s [%s %s]", curTime, r.RemoteAddr, r.Method, r.URL))

	// For response body
	// - Request Info
	fmt.Fprintln(w, "=== Request Information ===")
	fmt.Fprintln(w, fmt.Sprintf("\tMethod: %s", r.Method))
	fmt.Fprintln(w, fmt.Sprintf("\tURL: %s", r.URL))
	fmt.Fprintln(w, fmt.Sprintf("\tHost: %s", r.Host))
	fmt.Fprintln(w, fmt.Sprintf("\tRemoteAddr: %s", r.RemoteAddr))
	fmt.Fprintln(w, fmt.Sprintf("\tRequestURI: %s", r.RequestURI))
	fmt.Fprintln(w, fmt.Sprintf("\tContentLength: %d", r.ContentLength))
	fmt.Fprintln(w)

	// - Request Header
	fmt.Fprintln(w, "=== Request Headers ===")
	// Sorting
	headerKeys := make([]string, len(r.Header))
	i := 0
	for k := range r.Header {
		headerKeys[i] = k
		i++
	}
	sort.Strings(headerKeys)
	for _, k := range headerKeys {
		fmt.Fprintln(w, fmt.Sprintf("\t%s: %s", k, r.Header.Get(k)))
	}
	fmt.Fprintln(w)

	// - Request Body
	fmt.Fprintln(w, "=== Request Body ===")
	if r.ContentLength == 0 {
		fmt.Fprintln(w, "\t-- Empty Body --")
	} else {
		buf := make([]byte, 4)
		for {
			n, err := r.Body.Read(buf)
			fmt.Fprintf(w, fmt.Sprintf(string(buf[:n])))
			if err == io.EOF {
				fmt.Fprintln(w)
				break
			}
		}
	}
	fmt.Fprintln(w)

	// - Environment
	fmt.Fprintln(w, "=== Environment Variables ===")
	envvars := os.Environ()
	sort.Strings(envvars)
	for _, v := range envvars {
		fmt.Fprintln(w, fmt.Sprintf("\t%s", v))
	}
}
