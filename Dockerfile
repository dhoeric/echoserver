FROM golang:1.9.3-alpine

ENV WORKDIR /go/src/gitlab.com/dhoeric/echoserver
WORKDIR ${WORKDIR}

ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# For smaller image
FROM alpine:latest
WORKDIR /data
COPY --from=0 /go/src/gitlab.com/dhoeric/echoserver/app .
EXPOSE 8080
CMD /data/app
