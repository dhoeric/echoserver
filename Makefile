.PHONY: all build push
ORG:=dhoeric
NAME:=echoserver
TAG:=latest

all: build push

build:
	docker build -t $(ORG)/$(NAME):$(TAG) .

push:
	docker push $(ORG)/$(NAME):$(TAG)
